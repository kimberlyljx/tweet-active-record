require 'sqlite3'
require_relative 'models/legislator.rb'
require_relative 'models/rep.rb'
require_relative 'models/sen.rb'
require 'twitter'

require_relative 'models/tweet.rb'

Sen.create(title: 'Sen')
Rep.create(title: 'Rep')

client = Twitter::REST::Client.new do |config|
  config.consumer_key        = "uZ4nHoCgmaTSZkxc4AT54sDma"
  config.consumer_secret     = "0LYPZzrCOcaNks2ujVGBS2CY2KtvtRz4vJS7s5vQdO6HydWDuo"
  config.access_token        = "3042938911-oBpyL6hk0yVWpGxiudyREEWqXOYvQ3QOLAtwNP8"
  config.access_token_secret = "k91P3fCcZ8dvhSAy8XmXnogOKxjplvVDrlmTwdn8vtcEm"
end

puts "Which congressman do you want to see tweets by? Give id."
input = gets.chomp
  @legislator = Legislator.find_by(id: input)
  @sen_twitter_id = @legislator[:twitter_id]
  if @sen_twitter_id.empty?

  puts "This congressman has no Twitter"
  else
    x = client.user_timeline(@sen_twitter_id).take(10)
    x.each_with_index do |tweet,index|

    puts "#{index+1}.  #{client.status(tweet).text}"
      Tweet.create(text: tweet.text, unique_tweet_id: tweet.id,
        legislator_id: @legislator.id)
    end
  end

puts "Enter a state"
input = gets.chomp

puts "Senators:"
Sen.where(state: input).order(:lastname).reverse_order.each do |senator|
  puts "#{senator.name} (#{senator[:party]})"
end
puts "Representatives:"
Rep.where(state: input).order(:lastname).reverse_order.each do |senator|
  puts "#{senator.name} (#{senator[:party]})"
end

puts "Enter a gender"
input = gets.chomp

puts "#{input.capitalize} Senators :#{Sen.where(in_office: 1, gender: input).count} ( #{Sen.where(in_office: 1, gender: input).count*100/Sen.where(in_office: 1).count}% )"

puts "#{input.capitalize} Representatives :#{Rep.where(in_office: 1, gender: input).count} ( #{Rep.where(in_office: 1, gender: input).count*100/Rep.where(in_office: 1).count}% )"

Legislator.where(in_office: 1).group('state').order('count_id desc').count('id').each do |pairs|
  puts "#{pairs[0]}: #{Sen.where(in_office: 1, state: pairs[0]).count} Senators, #{Rep.where(in_office: 1, state: pairs[0]).count} Representative(s)"
end

puts "Senators: #{Sen.count}"
puts "Representatives: #{Rep.count}"

Legislator.where(in_office: 0).each do |x|
  x.destroy
end
